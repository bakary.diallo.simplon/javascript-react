import React, {Component} from 'react';
import cssModule from "./cssModule/text.module.css"
import styles from "./style";

class Index extends Component {
    render() {
        return (
            <div className={cssModule.container}>
                <h1 style={styles.h1} className={cssModule.message}>Title</h1>
            </div>
        )
    }
}

export default Index;
