This project, designed with the react framework, allowed me to implement and deepen the concepts of the hooks of the
react library.

- useState
- useEffect
- useReducer
- useCallback
- api Context

  
Run the following command to run the app


  - git clone https://gitlab.com/bakary.diallo.simplon/javascript-react.git
  - cd /e-commerce
  - yarn install
  - yarn start

App is running in ``http://localhost:3000/``




